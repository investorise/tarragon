import asyncio
from tqdm import tqdm
from tarragon.exception import PromiseException


class Promise(object):
    @staticmethod
    async def _internal_worker(semaphore, task):
        """
        This is internal worker that uses semaphore to control concurrency
        :param semaphore:
        :param task:
        :return:
        """
        async with semaphore:
            return await task

    @staticmethod
    async def all(__tasks: list, concurrency: int = 10, progress: object = None) -> list:
        """
        Runs thru the list of tasks asynchronously by limiting the concurrency b using a semaphore
        :param __tasks:
        :param concurrency:
        :param progress:
        :return:
        """
        try:
            semaphore = asyncio.Semaphore(concurrency)
            # re-schedule tasks
            re_tasks = list()
            for _task in __tasks:
                task = asyncio.ensure_future(Promise._internal_worker(semaphore, _task))
                re_tasks.append(task)

            if progress:
                progress_message = progress if isinstance(progress, str) else "Promise.all"
                results = [
                    await res for res in
                    tqdm(asyncio.as_completed(re_tasks),
                         desc=progress_message,
                         total=len(re_tasks))
                ]
            else:
                results = [await res for res in asyncio.as_completed(re_tasks)]
            return results

        except PromiseException as ex:
            print(ex)
            raise

    @staticmethod
    async def props(__props: dict, concurrency: int = 10, progress: object = None) -> dict:
        """
        Runs thru the dict of key,task asynchronously by limiting the concurrency b using a semaphore.
        Maps results back to the dictionary with same keys with all tasks fulfilled.
        It will fail if any task fails
        :param __tasks:
        :param concurrency:
        :param progress:
        :return:
        """
        try:
            semaphore = asyncio.Semaphore(concurrency)
            # re-schedule tasks
            re_tasks = list()
            keymap = list()
            for _key, _task in __props.items():
                task = asyncio.ensure_future(Promise._internal_worker(semaphore, _task))
                re_tasks.append(task)
                keymap.append(_key)

            if progress:
                progress_message = progress if isinstance(progress, str) else "Promise.all"
                results = [
                    await res for res in
                    tqdm(asyncio.as_completed(re_tasks),
                         desc=progress_message,
                         total=len(re_tasks))
                ]
            else:
                results = [await res for res in asyncio.as_completed(re_tasks)]
            # map results to output dictionary
            out = dict()
            for index, res in enumerate(results):
                _key = keymap[index]
                out[_key] = res
            return out

        except PromiseException as ex:
            print(ex)
            raise
