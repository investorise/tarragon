# 1.1.0

- Added support for tasks in dictionaries. 

# 1.0.1

- Custom progress message

# 1.0.0

- Initial version of Tarragon, the Python async promise library
