import os
from setuptools import setup, find_packages
from tarragon.util.version import find_version


def read_requirements():
    """Parse requirements from requirements.txt."""
    reqs_path = os.path.join('.', 'requirements.txt')
    with open(reqs_path, 'r') as f:
        requirements = [line.rstrip() for line in f]
    return requirements


setup(
    name="tarragon",
    version=find_version(os.getcwd(), "tarragon/version.py"),
    description="Tarragon - the async/promise library for Python 3",
    url="https://github.com/svenvarkel/tarragon",
    author="Sven Varkel",
    author_email="sven@investorise.com",
    license="MIT",
    packages=find_packages(),
    install_requires=read_requirements(),
    include_package_data=True,
    zip_safe=False
)
